APP_NAME := bernoulli-rust

image: Dockerfile
	docker build --rm -t $(APP_NAME) .

run:
	docker run --rm -it $(APP_NAME) $(APP_NAME) $(ARGS)

clean:
	docker rmi -f $(APP_NAME)
	$(MAKE) prune

kill:
	docker ps -a | tail -n +2 | cut -f1 -d' ' | xargs -r docker rm -f
	$(MAKE) prune

prune:
	docker image prune -f