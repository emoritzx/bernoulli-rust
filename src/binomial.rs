use ::factorial::Factorial;

pub fn binomial(n: u32, k: u32) -> f64 {
    let numerator = (n as u128).factorial();
    let denominator = (k as u128).factorial() * (n as u128 - k as u128).factorial();
    numerator as f64 / denominator as f64
}