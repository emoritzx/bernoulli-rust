use ::factorial::Factorial;
use bernoulli::bernoulli_recursive;

pub fn powersum_bernoulli(n: u32, m: u32) -> f64 {
    let mut sum : f64 = 0.0;
    for k in 0..m+1 {
        let b = bernoulli_recursive(k);
        let term = m + 1 - k;
        let numerator = b * n.pow(term) as f64;
        let denominator = (k as u128).factorial() * (term as u128).factorial();
        sum += numerator / denominator as f64;
    }
    (m as u128).factorial() as f64 * sum
}

pub fn powersum_naive(n: u32, m: u32) -> f64 {
    let mut sum = 0.0;
    for k in 0..n+1 {
        sum += k.pow(m) as f64;
    }
    sum
}