#[macro_use]
extern crate clap;
extern crate factorial;
#[cfg(test)]
#[macro_use]
extern crate float_cmp;

mod bernoulli;
mod binomial;
mod powersum;

use clap::App;
use powersum::*;

fn main() {
    let yaml = load_yaml!("cli.yaml");
    let matches = App::from_yaml(yaml)
        .get_matches();
    if let Some(matches) = matches.subcommand_matches("sum") {
        let range = matches.value_of("range").unwrap()
            .parse::<u32>().unwrap();
        let power = matches.value_of("power").unwrap()
            .parse::<u32>().unwrap();
        println!("Summing the first {} integers to the power of {}", range, power);
        println!("Answer: {}", powersum_bernoulli(range, power));
    }
    if let Some(matches) = matches.subcommand_matches("sum-naive") {
        let range = matches.value_of("range").unwrap()
            .parse::<u32>().unwrap();
        let power = matches.value_of("power").unwrap()
            .parse::<u32>().unwrap();
        println!("Summing the first {} integers to the power of {}", range, power);
        println!("Answer: {}", powersum_naive(range, power));
    }
}