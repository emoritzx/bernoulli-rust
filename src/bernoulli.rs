use binomial::binomial;

const LOOKUP_TABLE : [f64; 21] = [ 1., 0.5, 1./6., 0., -1./30., 0., 1./42., 0., -1./30., 0., 5./66., 0., -691./2730., 0., 7./6., 0., -3617./510., 0., 43867./798., 0., -174611./330. ];

pub fn bernoulli_recursive(m: u32) -> f64 {
    if m == 0 {
        return 1.0
    }
    if m > 1 && m % 2 == 1 {
        return 0.0
    }
    let mut sum : f64 = 0.0;
    for k in 0..m {
        let b = bernoulli_recursive(k);
        let coefficient = binomial(m, k);
        let term = m - k + 1;
        let iteration = coefficient * (b / term as f64);
        sum += iteration;
    }
    1.0 - sum
}

pub fn bernoulli_lookup(m: u32) -> f64 {
    LOOKUP_TABLE[m as usize]
}

#[cfg(test)]
mod tests {

    #[test]
    fn test_against_precomputed_numbers() {
        for k in 0..super::LOOKUP_TABLE.len() {
            let actual = super::bernoulli_recursive(k as u32);
            let expected = super::bernoulli_lookup(k as u32);
            assert!(approx_eq!(f64, actual, expected, epsilon = 1e-6), "{} == {} at index {}", actual, expected, k);
        }
    }
}