FROM rust:latest as builder
WORKDIR /usr/src/app

# cache crate downloads
COPY Cargo.toml .
RUN mkdir .cargo
RUN cargo vendor > .cargo/config

# build application
COPY ./src src
RUN cargo test
RUN cargo install --path .

# create runtime image
FROM debian:buster-slim
COPY --from=builder /usr/local/cargo/bin/bernoulli-rust /usr/local/bin/bernoulli-rust
CMD ["bernoulli-rust"]